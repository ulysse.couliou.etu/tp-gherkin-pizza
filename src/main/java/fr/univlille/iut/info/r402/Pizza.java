package fr.univlille.iut.info.r402;

public abstract class Pizza {

    boolean cuite;
    boolean vendable;
    final int PRIX_PIZZA = 12;

    public abstract String getNom();

    public boolean estCuite() {
        return cuite;
    }

    public void setCuite(boolean cuite) {
        this.cuite = cuite;
    }

    public boolean estVendable() {
        if (cuite == true) {
            vendable = true;
        } else {
            vendable = false;
        }
        return vendable;
    }

}

