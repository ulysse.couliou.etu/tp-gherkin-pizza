package fr.univlille.iut.info.r402;

public class Pizzaiolo {

    int montant_caisse = 1000;

    public void preparerPizza(String pizza) {
    }
    public void mettreAuFour() {
    }

    public void mettreALaPoubelle() {

    }

    public void vendrePizza(Pizza pizza) {
        if(pizza.estCuite()){
            montant_caisse += pizza.PRIX_PIZZA;
            System.out.println("La pizza est vendu au prix de "+pizza.PRIX_PIZZA+"€.");
        }
        else{
            System.out.println("La pizza n'est pas cuite ou ne peux pas être vendue.");
        }
    }

    public void mettreALaPoubelle(Pizza pizza) {
        if (pizza.estVendable()) {
            System.out.println("On peut vendre la pizza.");
        }
        else{
            System.out.println("On jette la pizza à la poubelle.");
        }
    }

}
