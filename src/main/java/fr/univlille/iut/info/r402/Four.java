package fr.univlille.iut.info.r402;

public class Four {

    final int CAPACITE = 3;
    int nbPizzasDansLeFour;
    public void cuire(Pizza pizza, int ticks) {
        if (nbPizzasDansLeFour < CAPACITE){
            nbPizzasDansLeFour++;
            if (ticks>=30 && ticks <60){
                pizza.setCuite(true);
            }
            else{
                pizza.setCuite(false);
            }
        }

        else{
            pizza.setCuite(false);
        }
    }



}

