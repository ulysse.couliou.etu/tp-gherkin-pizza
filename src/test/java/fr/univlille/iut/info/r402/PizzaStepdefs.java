package fr.univlille.iut.info.r402;


import io.cucumber.java.en.*;

public class PizzaStepdefs {
    @Given("^un four")
    public void unFour() {
    }

    @And("^un pizzaiolo")
    public void unPizzaiolo() {
    }

    @And("^le pizzaiolo prépare une pizza reine")
    public void preparerReine() {
    }
    @When("^le pizzaiolo met la pizza reine au four$")
    public void pizzaAuFour() {
    }

    @Then("au bout de {int} ticks d'horloge, la pizza est cuite")
    public void pizzaCuite(int ticks) {
    }
}

